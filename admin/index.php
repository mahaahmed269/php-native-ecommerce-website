<?php 

 include "init.php";
 include $tpl . 'header.php';
 include 'includes/languages/english.php';

 if($_SERVER['REQUEST_METHOD']=='POST'){
 	$username= $_POST['username'];
 	$password=$_POST['password'];
 	$hashedPass=sha1($password);

 	$stmt = $conn->prepare("SELECT username , password FROM users WHERE username= ? AND password =? ");
 	$stmt->execute(array($username , $hashedPass));
 	$count = $stmt->rowCount();
 	echo $count;
 	 if($count > 0){
 	 	echo "welcome" . $username ;
 	 }
 }
 ?>

  <?php //----------------------------login form-----------------?>
  <br><br><br>
  <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form method="POST" class="login border-primary" action="<?php echo $_SERVER['PHP_SELF'] ?>">
                        <div class=" border-primary form-group row">
                            <label for="username" class="col-sm-4 col-form-label text-md-right">username</label>
                            <div class="col-md-6">
                                <input id="username" type="text" class="form-control" name="username" placeholder="username" autocomplete="off" />
                                    <span class="invalid-feedback" role="alert">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">password</label>
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" placeholder="password" autocomplete="off" />
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember">remember password
                                    <label class="form-check-label" for="remember"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-lg btn-primary"><a class="btn btn-link text-white" href="">login</a></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
  <?php //----------------------------end login form-----------------?>






 <?php include $tpl . 'footer.php'; ?>